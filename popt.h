/* 
 *  Option Parsing Library (POPT)
 *  Copyright (c) 1998-2002 Red Hat, Inc.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 *  X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 *  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 *  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 *  Except as contained in this notice, the name of the X Consortium shall not be
 *  used in advertising or otherwise to promote the sale, use or other dealings
 *  in this Software without prior written authorization from the X Consortium.
 *
 *  NOTICE:
 *  This is an automatically generated, stripped down version of the
 *  POPT 1.7 library from Red Hat, Inc. This version is still
 *  distributed under above Open Source license, but Red Hat is no longer
 *  responsible for this version. Contact The OSSP Project instead.
 */

#ifndef __POPT_H__
#define __POPT_H__

#include <stdio.h>

#define POPT_OPTION_DEPTH	10

#define POPT_ARG_NONE		0
#define POPT_ARG_STRING		1
#define POPT_ARG_INT		2
#define POPT_ARG_LONG		3
#define POPT_ARG_INCLUDE_TABLE	4
#define POPT_ARG_CALLBACK	5
#define POPT_ARG_INTL_DOMAIN    6
#define POPT_ARG_VAL		7
#define	POPT_ARG_FLOAT		8
#define	POPT_ARG_DOUBLE		9

#define POPT_ARG_MASK		0x0000FFFF

#define POPT_ARGFLAG_ONEDASH	0x80000000
#define POPT_ARGFLAG_DOC_HIDDEN 0x40000000
#define POPT_ARGFLAG_STRIP	0x20000000
#define	POPT_ARGFLAG_OPTIONAL	0x10000000

#define	POPT_ARGFLAG_OR		0x08000000
#define	POPT_ARGFLAG_NOR	0x09000000
#define	POPT_ARGFLAG_AND	0x04000000
#define	POPT_ARGFLAG_NAND	0x05000000
#define	POPT_ARGFLAG_XOR	0x02000000
#define	POPT_ARGFLAG_NOT	0x01000000
#define POPT_ARGFLAG_LOGICALOPS \
        (POPT_ARGFLAG_OR|POPT_ARGFLAG_AND|POPT_ARGFLAG_XOR)

#define	POPT_BIT_SET	(POPT_ARG_VAL|POPT_ARGFLAG_OR)

#define	POPT_BIT_CLR	(POPT_ARG_VAL|POPT_ARGFLAG_NAND)

#define	POPT_ARGFLAG_SHOW_DEFAULT 0x00800000

#define POPT_CBFLAG_PRE		0x80000000
#define POPT_CBFLAG_POST	0x40000000
#define POPT_CBFLAG_INC_DATA	0x20000000
#define POPT_CBFLAG_SKIPOPTION	0x10000000
#define POPT_CBFLAG_CONTINUE	0x08000000

#define POPT_ERROR_NOARG	-10
#define POPT_ERROR_BADOPT	-11
#define POPT_ERROR_OPTSTOODEEP	-13
#define POPT_ERROR_BADQUOTE	-15
#define POPT_ERROR_ERRNO	-16
#define POPT_ERROR_BADNUMBER	-17
#define POPT_ERROR_OVERFLOW	-18
#define	POPT_ERROR_BADOPERATION	-19
#define	POPT_ERROR_NULLARG	-20
#define	POPT_ERROR_MALLOC	-21

#define POPT_BADOPTION_NOALIAS  (1 << 0)

#define POPT_CONTEXT_NO_EXEC	(1 << 0)
#define POPT_CONTEXT_KEEP_FIRST	(1 << 1)
#define POPT_CONTEXT_POSIXMEHARDER (1 << 2)
#define POPT_CONTEXT_ARG_OPTS	(1 << 4)

struct popt_option {
	const char *longName;
	char shortName;
	int argInfo;
	void *arg;
	int val;
	const char *descrip;
	const char *argDescrip;
};

struct popt_alias {
	const char *longName;
	char shortName;
	int argc;
	const char **argv;
};

typedef struct popt_item_s {
	struct popt_option option;
	int argc;
	const char **argv;
}  *popt_item;

extern struct popt_option popt_aliasOptions[];

#define POPT_AUTOALIAS { NULL, '\0', POPT_ARG_INCLUDE_TABLE, popt_aliasOptions, \
			0, "Options implemented via popt alias/exec:", NULL },

extern struct popt_option popt_helpoptions[];

#define POPT_AUTOHELP { NULL, '\0', POPT_ARG_INCLUDE_TABLE, popt_helpoptions, \
			0, "Help options:", NULL },

#define POPT_TABLEEND { NULL, '\0', 0, 0, 0, NULL, NULL }

typedef struct popt_context_s *popt_context;

#ifndef __cplusplus

typedef struct popt_option *popt_option;

#endif

enum popt_callbackreason {
	POPT_CALLBACK_REASON_PRE = 0,
	POPT_CALLBACK_REASON_POST = 1,
	POPT_CALLBACK_REASON_OPTION = 2
};

#ifdef __cplusplus
extern "C" {
#endif

	typedef void (*popt_callbacktype) (popt_context con,
									   enum popt_callbackreason reason,
									   const struct popt_option * opt,
									   const char *arg, const void *data);

	popt_context popt_getcontext(const char *name,
								 int argc, const char **argv,
								 const struct popt_option *options,
								 int flags);

	void popt_resetcontext(popt_context con);

	int popt_getnextopt(popt_context con)
	   ;

	const char *popt_getoptarg(popt_context con);

	const char *popt_getarg(popt_context con);

	const char *popt_peekarg(popt_context con);

	const char **popt_getargs(popt_context con);

	const char *popt_badoption(popt_context con, int flags);

	popt_context popt_freecontext(popt_context con);

	int popt_stuffargs(popt_context con, const char **argv);

	int popt_addalias(popt_context con, struct popt_alias alias, int flags);

	int popt_additem(popt_context con, popt_item newItem, int flags);

	int popt_readconfigfile(popt_context con, const char *fn)
	   ;

	int popt_readdefaultconfig(popt_context con, int useEnv)
	   ;

	int popt_dupargv(int argc, const char **argv,
					 int *argcPtr, const char ***argvPtr);

	int popt_parseargvstring(const char *s,
							 int *argcPtr, const char ***argvPtr);

	int poptConfigFileToString(FILE * fp, char **argstrp, int flags)
	   ;

	const char *const popt_strerror(const int error);

	void popt_setexecpath(popt_context con, const char *path,
						  int allowAbsolute);

	void popt_printhelp(popt_context con, FILE * fp, int flags)
	   ;

	void popt_printusage(popt_context con, FILE * fp, int flags)
	   ;

	void popt_setotheroptionhelp(popt_context con, const char *text);

	const char *popt_getinvocationname(popt_context con);

	int popt_strippedargv(popt_context con, int argc, char **argv);

	int poptSaveLong(long *arg, int argInfo, long aLong)
	   ;

	int poptSaveInt(int *arg, int argInfo, long aLong)
	   ;

#ifdef  __cplusplus
}
#endif
#endif
