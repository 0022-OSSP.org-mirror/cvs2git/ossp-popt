##
##  OSSP popt - Option Parsing Library
##  Copyright (c) 2002 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2002 The OSSP Project <http://www.ossp.org/>
##  Copyright (c) 2002 Cable & Wireless Deutschland <http://www.cw.com/de/>
##
##  This file is part of OSSP popt, an option parsing library
##  which can be found at http://www.ossp.org/pkg/popt/.
##
##  This program is free software; you can redistribute it and/or
##  modify it under the terms of the GNU General Public  License
##  as published by the Free Software Foundation; either version
##  2.0 of the License, or (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this file; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
##  USA, or contact the OSSP project <ossp@ossp.org>.
##
##  Makefile.in: make(1) build procedure
##

@SET_MAKE@

DESTDIR     =
prefix      = @prefix@
exec_prefix = @exec_prefix@
bindir      = @bindir@
libdir      = @libdir@
includedir  = @includedir@
mandir      = @mandir@

CC          = @CC@
CPPFLAGS    = @CPPFLAGS@
CFLAGS      = @DEFS@ @CFLAGS@
LDFLAGS     = @LDFLAGS@
LIBS        = @LIBS@
RM          = rm -f
RMDIR       = rmdir
SHTOOL      = ./shtool
LIBTOOL     = ./libtool
TRUE        = true

LIB_NAME    = libpopt.la
LIB_OBJS    = popt.lo

TST_NAME    = popt_test
TST_OBJS    = popt_test.o

.SUFFIXES:
.SUFFIXES: .c .o .lo

all: $(LIB_NAME) $(TST_NAME)

.c.o:
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $<

.c.lo:
	@$(LIBTOOL) --mode=compile $(CC) $(CPPFLAGS) $(CFLAGS) -c $<

$(LIB_OBJS): Makefile
$(TST_OBJS): Makefile

$(LIB_NAME): $(LIB_OBJS)
	@$(LIBTOOL) --mode=link $(CC) -o $(LIB_NAME) $(LIB_OBJS) -rpath $(libdir) \
	    -version-info `$(SHTOOL) version -l txt -d libtool VERSION`

$(TST_NAME): $(TST_OBJS) $(LIB_NAME)
	@$(LIBTOOL) --mode=link $(CC) $(LDFLAGS) -o $(TST_NAME) $(TST_OBJS) $(LIB_NAME) $(LIBS)

check: $(TST_NAME)
	@$(LIBTOOL) --mode=execute ./$(TST_NAME)

install:
	$(SHTOOL) mkdir -f -p -m 755 $(DESTDIR)$(prefix)
	$(SHTOOL) mkdir -f -p -m 755 $(DESTDIR)$(bindir)
	$(SHTOOL) mkdir -f -p -m 755 $(DESTDIR)$(includedir)
	$(SHTOOL) mkdir -f -p -m 755 $(DESTDIR)$(libdir)
	$(SHTOOL) install -c -m 755 popt-config $(DESTDIR)$(bindir)/
	$(SHTOOL) install -c -m 644 popt.h $(DESTDIR)$(includedir)/
	@$(LIBTOOL) --mode=install $(SHTOOL) install -c -m 644 libpopt.la $(DESTDIR)$(libdir)/

uninstall:
	@$(LIBTOOL) --mode=uninstall $(RM) $(DESTDIR)$(libdir)/libpopt.la
	-$(RM) $(DESTDIR)$(includedir)/popt.h
	-$(RM) $(DESTDIR)$(bindir)/popt-config
	-$(RMDIR) $(DESTDIR)$(libdir) >/dev/null 2>&1 || $(TRUE)
	-$(RMDIR) $(DESTDIR)$(includedir) >/dev/null 2>&1 || $(TRUE)
	-$(RMDIR) $(DESTDIR)$(bindir) >/dev/null 2>&1 || $(TRUE)
	-$(RMDIR) $(DESTDIR)$(prefix) >/dev/null 2>&1 || $(TRUE)

clean:
	-$(RM) $(LIB_NAME) $(LIB_OBJS)
	-$(RM) $(TST_NAME) $(TST_OBJS)
	-$(RM) -r .libs >/dev/null 2>&1 || $(TRUE)
	-$(RM) *.o *.lo

distclean: clean
	-$(RM) config.log config.status config.cache
	-$(RM) Makefile config.h popt-config
	-$(RM) libtool

realclean: distclean
	-$(RM) configure config.h.in
	-$(RM) shtool
	-$(RM) ltmain.sh libtool.m4 config.guess config.sub

